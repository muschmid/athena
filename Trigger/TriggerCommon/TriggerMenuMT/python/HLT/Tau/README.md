Modules in this directory
-----

* [GenerateTauChainDefs](GenerateTauChainDefs.py)
  * Called by the menu code to pass the chain dict into the ChainConfiguration object
* [TauChainConfiguration](TauChainConfiguration.py)
  * Defines the ChainConfiguration object that interprets the chain dict and builds the chain
* [TauMenuSequences](TauMenuSequences.py)
  * Defines the top-level sequences containing the input maker and hypothesis alg
* [TauConfigurationTools](TauConfigurationTools.py)
  * Defines the tools used by multiple modules to parse the Tau Trigger configuration
