/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Baptiste Ravina

#ifndef TRUTH__PARTICLE_LEVEL_ANALYSIS_ALGORITHMS__DICT_H
#define TRUTH__PARTICLE_LEVEL_ANALYSIS_ALGORITHMS__DICT_H

#include <TruthParticleLevelAnalysisAlgorithms/ParticleLevelChargeDecoratorAlg.h>
#include <TruthParticleLevelAnalysisAlgorithms/ParticleLevelIsolationAlg.h>
#include <TruthParticleLevelAnalysisAlgorithms/ParticleLevelJetsAlg.h>
#include <TruthParticleLevelAnalysisAlgorithms/ParticleLevelMissingETAlg.h>
#include <TruthParticleLevelAnalysisAlgorithms/ParticleLevelOverlapRemovalAlg.h>
#include <TruthParticleLevelAnalysisAlgorithms/ParticleLevelPtEtaPhiDecoratorAlg.h>

#endif
