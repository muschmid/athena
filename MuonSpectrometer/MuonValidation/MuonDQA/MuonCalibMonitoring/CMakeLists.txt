# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( MuonCalibMonitoring )

# External dependencies:
find_package( ROOT COMPONENTS Core Hist Graf )

# Component(s) in the package:
atlas_add_component( MuonCalibMonitoring
                     src/*.cxx
                     src/components/*.cxx
                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                     LINK_LIBRARIES ${ROOT_LIBRARIES} AthenaMonitoringLib CxxUtils StoreGateLib GaudiKernel CscCalibData MuonCondData MuonCondInterface MuonIdHelpersLib )
